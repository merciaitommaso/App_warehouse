#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "rifornimentodialog.h"


#include <QMainWindow>
#include <QFile>
#include <QtSql/QSqlDatabase>
#include <QDebug>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QTableView>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public:
    bool connect_to_db();
    void set_punte_model();
    void connect_slots();
    void update_db();
    void cerca_quantita_basse();








public slots:
    void login_rifornimento_magazzino();
    void put_diametri_in_box_diametri();
    void put_materiale_in_box_mariali();
    void put_qta_in_label_qta_and_set_query_model();
    void prelieva_funct();
    void add_qta_funct();
    void exportTableViewToCSV(QTableView *table);
    void exportTableViewToCSV_connect();
    void put_oggetti_in_box_oggetti();




private:
    Ui::MainWindow *ui;

    QSqlDatabase db;
    QSqlTableModel *model_magazzino;
    rifornimentoDialog *rifornimento_magazzino_dialog;
    QSqlQuery query;
    QSqlQueryModel *model;
    QTableView *view_to_print_CSV;

};

#endif // MAINWINDOW_H
