#-------------------------------------------------
#
# Project created by QtCreator 2018-06-18T14:55:17
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = App_Magazzino
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    rifornimentodialog.cpp

HEADERS  += mainwindow.h \
    rifornimentodialog.h

FORMS    += mainwindow.ui \
    rifornimentodialog.ui

RESOURCES += \
    db_magazzino.qrc \
    logo.qrc \
    plus_logo.qrc
