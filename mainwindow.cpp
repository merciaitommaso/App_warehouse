#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    connect_to_db();
    set_punte_model();
    connect_slots();
    update_db();
    put_oggetti_in_box_oggetti();
    cerca_quantita_basse();






}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::login_rifornimento_magazzino()
{
    rifornimento_magazzino_dialog = new rifornimentoDialog(db, model_magazzino, this);
    rifornimento_magazzino_dialog->exec();
}



bool MainWindow::connect_to_db()
{
    QFile *db_file = new QFile(":/db/db_magazzino");
    db_file->setPermissions(QFile::ExeGroup | QFile::ExeOther|QFile::ExeOwner| QFile::ExeUser|QFile::ReadOwner|QFile::ReadOther|QFile::ReadGroup|QFile::ReadUser|QFile::WriteOwner|QFile::WriteOther|QFile::WriteGroup|QFile::WriteUser);
    db_file->copy(":/db/db_magazzino", "./db_magazzino");

    QFile* db_file_copied = new QFile("./db_magazzino");
    db_file_copied->setPermissions(QFile::ExeGroup | QFile::ExeOther|QFile::ExeOwner| QFile::ExeUser|QFile::ReadOwner|QFile::ReadOther|QFile::ReadGroup|QFile::ReadUser|QFile::WriteOwner|QFile::WriteOther|QFile::WriteGroup|QFile::WriteUser);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("");
    db.setDatabaseName("./db_magazzino");
    db.setUserName("");
    db.setPassword("");

    qDebug() << "db_connected" << db.open();

    return db.open();
}



void MainWindow::update_db()
{
    QFile* db_file = new QFile("./db_magazzino");
    db_file->copy("./db_manage",":/db/db_magazzino");
    db_file->setPermissions(QFile::ExeGroup | QFile::ExeOther|QFile::ExeOwner| QFile::ExeUser|QFile::ReadOwner|QFile::ReadOther|QFile::ReadGroup|QFile::ReadUser|QFile::WriteOwner|QFile::WriteOther|QFile::WriteGroup|QFile::WriteUser);
    qDebug() <<"update";
}


void MainWindow::set_punte_model()
{
    model_magazzino = new QSqlTableModel(this,db);
    model_magazzino->setTable("Magazzino");
    model_magazzino->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model_magazzino->select();
    ui->tab_magazzino->setModel(model_magazzino);

}


void MainWindow::put_oggetti_in_box_oggetti()
{
    connect_to_db();

    ui->oggetti_Box->clear();

    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino");
    QStringList oggetti;
    QSet<QString> oggetti_no_duplicate;




    while (query.next()) {
        QString oggetto= query.value("oggetto").toString();
        oggetti.append(oggetto);
        qDebug()  << oggetto ;
    }


    oggetti_no_duplicate = oggetti.toSet();
    qDebug() << oggetti_no_duplicate.toList();
    ui->oggetti_Box->addItems(oggetti_no_duplicate.toList());
}


void MainWindow::put_diametri_in_box_diametri()
{
    ui->diametro_Box->clear();
    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "';");
    QStringList diametri;
    QSet<QString> diametri_no_duplicate;

    qDebug() << "SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "';";


    while (query.next()) {
        QString diametro= query.value("diametro").toString();
        diametri.append(diametro);
        qDebug()  << diametro ;
    }

    diametri_no_duplicate = diametri.toSet();
    qDebug() << diametri_no_duplicate;

    ui->diametro_Box->addItems(diametri_no_duplicate.toList());
}



void MainWindow::put_materiale_in_box_mariali()
{
    ui->materiale_Box->clear();
    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"';");
    QStringList materiali;
    QSet<QString> materiali_no_duplicate;




    while (query.next()) {
        QString materiale= query.value("materiale").toString();
        materiali.append(materiale);
        qDebug()  << materiale ;
    }

    materiali_no_duplicate = materiali.toSet();
    qDebug() << materiali_no_duplicate;
    ui->materiale_Box->addItems(materiali_no_duplicate.toList());
}


void MainWindow::put_qta_in_label_qta_and_set_query_model()
{

    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    QString quantita_s;
    QString photo;


//    qDebug() <<"SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';";


    while (query.next()) {
            quantita_s = query.value("quantita").toString();
            photo = query.value("nome_foto").toString();
            qDebug()  << quantita_s ;
    }

//    ui->qta_label->setText(quantita_s);

    model = new QSqlQueryModel();
    model->setQuery(query);
    ui->tab_magazzino->setModel(model);

    QPixmap mypix ("./foto/" + photo);
    ui->pixmap_label->setPixmap(mypix);


}


void MainWindow::prelieva_funct()
{
    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    QString quantita_s;
    QString aggiunta_s;
    QString result_s;
    float quantita_f;
    float aggiunta_f;
    float result_f;

//    qDebug() <<"SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';";


    while (query.next()) {
            quantita_s = query.value("quantita").toString();
            qDebug()  << quantita_s ;

    }



    quantita_f = quantita_s.toFloat();

    aggiunta_s = ui->prelev_qta_lineEdit->text();
    aggiunta_f = aggiunta_s.toFloat();

    result_f = quantita_f - aggiunta_f;
    result_s = QString::number(result_f);

    qDebug() << query.prepare("UPDATE Magazzino SET quantita='"+result_s+"' WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    query.prepare("UPDATE Magazzino SET quantita='"+result_s+"' WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");
    query.exec();

    model_magazzino->submitAll();
    model_magazzino->select();
    ui->tab_magazzino->setModel(model_magazzino);

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    model = new QSqlQueryModel();
    model->setQuery(query);
    ui->tab_magazzino->setModel(model);



}





void MainWindow::add_qta_funct()
{
    QSqlQuery query;

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    QString quantita_s;
    QString aggiunta_s;
    QString result_s;
    float quantita_f;
    float aggiunta_f;
    float result_f;

//    qDebug() <<"SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';";


    while (query.next()) {
            quantita_s = query.value("quantita").toString();
            qDebug()  << quantita_s ;

    }



    quantita_f = quantita_s.toFloat();

    aggiunta_s = ui->aggiungi_qta_lineEdit->text();
    aggiunta_f = aggiunta_s.toFloat();

    result_f = quantita_f + aggiunta_f;
    result_s = QString::number(result_f);

    qDebug() << query.prepare("UPDATE Magazzino SET quantita='"+result_s+"' WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    query.prepare("UPDATE Magazzino SET quantita='"+result_s+"' WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");
    query.exec();

    model_magazzino->submitAll();
    model_magazzino->select();
    ui->tab_magazzino->setModel(model_magazzino);

    query.exec("SELECT * FROM Magazzino WHERE oggetto='" +ui->oggetti_Box->currentText()+ "' AND diametro='"+ui->diametro_Box->currentText()+"' AND materiale='"+ui->materiale_Box->currentText()+"';");

    model = new QSqlQueryModel();
    model->setQuery(query);
    ui->tab_magazzino->setModel(model);



}


void MainWindow::cerca_quantita_basse()
{


        QString diametro;
        QString oggetto;
        QString quantita;
        QString text;
        QSqlQuery query;
        query.exec("SELECT * FROM Magazzino WHERE quantita < 3;");

        while(query.next())
        {
            oggetto = query.value("oggetto").toString();
            diametro = query.value("diametro").toString();
            quantita = query.value("quantita").toString();
            if(quantita.toFloat() <= 2 )
            {
                qDebug() <<"Rifornire " + oggetto +"  Ø= "+ diametro + "  q.tà= " + quantita;
                text = "Rifornire " + oggetto +"  Ø= "+ diametro + "  q.tà= " + quantita;
                ui->plainTextEdit->appendPlainText(text);
            }


         }


}







void MainWindow::exportTableViewToCSV(QTableView *table) {
         QString filters("CSV files (*.csv);;All files (*.*)");
         QString defaultFilter("CSV files (*.csv)");
         QString fileName = QFileDialog::getSaveFileName(0, "Save file", QCoreApplication::applicationDirPath(),
                            filters, &defaultFilter);
         QFile file(fileName);

         QAbstractItemModel *model =  table->model();
         if (file.open(QFile::WriteOnly | QFile::Truncate)) {
             QTextStream data(&file);
             QStringList strList;
             for (int i = 0; i < model->columnCount(); i++) {
                 if (model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString().length() > 0)
                     strList.append("\"" + model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString() + "\"");
                 else
                     strList.append("");
             }
             data << strList.join(";") << "\n";
             for (int i = 0; i < model->rowCount(); i++) {
                 strList.clear();
                 for (int j = 0; j < model->columnCount(); j++) {

                     if (model->data(model->index(i, j)).toString().length() > 0)
                         strList.append("\"" + model->data(model->index(i, j)).toString() + "\"");
                     else
                         strList.append("");
                 }
                 data << strList.join(";") + "\n";
             }
             file.close();
         }

     }

void MainWindow::exportTableViewToCSV_connect()
{

        view_to_print_CSV = new QTableView;
        view_to_print_CSV->setModel(model_magazzino);
        exportTableViewToCSV(view_to_print_CSV);
}












void MainWindow::connect_slots()
{
    QObject::connect(ui->rif_magazzinoButton, SIGNAL(clicked(bool)), this, SLOT(login_rifornimento_magazzino()));
    QObject::connect(ui->refreshButton, SIGNAL(clicked(bool)), this, SLOT(put_oggetti_in_box_oggetti()));
    QObject::connect(ui->oggetti_Box, SIGNAL(currentIndexChanged(int)), this, SLOT(put_diametri_in_box_diametri()));
    QObject::connect(ui->diametro_Box, SIGNAL(currentIndexChanged(int)), this, SLOT(put_materiale_in_box_mariali()));
    QObject::connect(ui->find_Button, SIGNAL(clicked(bool)), this, SLOT(put_qta_in_label_qta_and_set_query_model()));
    QObject::connect(ui->preleva_Button, SIGNAL(clicked(bool)), this, SLOT(prelieva_funct()));
    QObject::connect(ui->aggiungi_Button, SIGNAL(clicked(bool)), this, SLOT(add_qta_funct()));
    QObject::connect(ui->exportButton, SIGNAL(clicked(bool)), this, SLOT(exportTableViewToCSV_connect()));
}
