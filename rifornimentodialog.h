#ifndef RIFORNIMENTODIALOG_H
#define RIFORNIMENTODIALOG_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlTableModel>

namespace Ui {
class rifornimentoDialog;
}

class rifornimentoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit rifornimentoDialog(QSqlDatabase db, QSqlTableModel *model_magazzino, QWidget *parent = 0);
    ~rifornimentoDialog();


public:
    void connect_slots();
    void set_magazzino_model();


public slots:

    void add_oggetti();
    void cancel_oggetti();


private:
    Ui::rifornimentoDialog *ui;
    QSqlDatabase db;
    QSqlTableModel *model_magazzino;
};

#endif // RIFORNIMENTODIALOG_H
