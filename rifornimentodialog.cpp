#include "rifornimentodialog.h"
#include "ui_rifornimentodialog.h"

rifornimentoDialog::rifornimentoDialog(QSqlDatabase db, QSqlTableModel *model_magazzino, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::rifornimentoDialog)
{
    ui->setupUi(this);
    this->db = db;
    this->model_magazzino = model_magazzino;
    set_magazzino_model();
    connect_slots();
}

rifornimentoDialog::~rifornimentoDialog()
{
    delete ui;
}


void rifornimentoDialog::set_magazzino_model()
{
    ui->magazzino_tableView->setModel(model_magazzino);
}


void rifornimentoDialog::add_oggetti()
{
    model_magazzino->insertRows(model_magazzino->rowCount(),1);
    int row = model_magazzino->rowCount()-1;

    model_magazzino->setData(model_magazzino->index(row,1), ui->oggetto_lineEdit->text().trimmed().toUpper());
    model_magazzino->setData(model_magazzino->index(row,2), ui->qta_lineEdit->text().trimmed().toInt());
    model_magazzino->setData(model_magazzino->index(row,3), ui->diametro_lineEdit->text().trimmed());
    model_magazzino->setData(model_magazzino->index(row,4), ui->materiale_lineEdit->text().trimmed().toUpper());
    model_magazzino->setData(model_magazzino->index(row,5), ui->descrizione_lineEdit->text().trimmed().toUpper());
    model_magazzino->setData(model_magazzino->index(row,6), ui->nome_foto_lineEdit->text());

    model_magazzino->submitAll();

    model_magazzino->select();

    ui->magazzino_tableView->setModel(model_magazzino);

}

void rifornimentoDialog::cancel_oggetti()
{
    QItemSelection selection( ui->magazzino_tableView->selectionModel()->selection() );

    QList<int> rows;
    foreach( const QModelIndex & index, selection.indexes() ) {
       rows.append( index.row() );
    }

    qSort( rows );

    int prev = -1;
    for( int i = rows.count() - 1; i >= 0; i -= 1 )
    {
       int current = rows[i];
       if( current != prev ) {
          model_magazzino->removeRows( current, 1 );
          prev = current;
       }
    }


    model_magazzino->submitAll();
    model_magazzino->select();
    ui->magazzino_tableView->setModel(model_magazzino);
}



void rifornimentoDialog::connect_slots()
{
    connect(ui->add_Button, SIGNAL(clicked(bool)), this, SLOT(add_oggetti()));
    connect(ui->cancel_Button, SIGNAL(clicked(bool)), this, SLOT(cancel_oggetti()));
}
